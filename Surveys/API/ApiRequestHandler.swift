//
//  OAuth2Handler.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 28/11/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Alamofire

final class ApiRequestHandler: RequestAdapter, RequestRetrier {
    
    /// Completion handler when refreshing access token
    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?) -> Void
    
    public let sessionManager: SessionManager
    
    private let lock = NSLock()
    
    private var accessToken: String
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    
    // MARK: - Initialization
    
    public init() {
        // Retrive from saved accessToken
        self.accessToken = KeychainHelper.get(key: Constants.KeyChain.accessTokenKey)
        
        self.sessionManager = SessionManager()
        self.sessionManager.adapter = self
        self.sessionManager.retrier = self
    }
    
    // MARK: - RequestAdapter
    
    /// Includes access token with every request
    ///
    /// - Parameter urlRequest: Initial urlRequest
    /// - Returns: urlRequest with access token in header
    /// - Throws:
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        if (urlRequest.url?.absoluteString.hasPrefix(Constants.API.baseUrl))! {
            var urlRequest = urlRequest
            urlRequest.setValue(Constants.API.HEADER.AUTHORIZATION.prefix + accessToken, forHTTPHeaderField: Constants.API.HEADER.AUTHORIZATION.name)
            return urlRequest
        }
        
        return urlRequest
    }
    
    // MARK: - RequestRetrier
    
    /// Executes requests to server and refresh access token if invalid
    ///
    /// - Parameters:
    ///   - manager: SessionManager
    ///   - request: Request for data
    ///   - error: Error if any
    ///   - completion: RequestRetryCompletion handler block
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
        
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            requestsToRetry.append(completion)
            
            if !isRefreshing {
                refreshToken { [weak self] succeeded, accessToken in
                    guard let strongSelf = self else { return }
                    
                    strongSelf.lock.lock() ; defer { strongSelf.lock.unlock() }
                    
                    if let accessToken = accessToken {
                        KeychainHelper.set(key: Constants.KeyChain.accessTokenKey, value: accessToken)
                        strongSelf.accessToken = accessToken
                    }
                    
                    strongSelf.requestsToRetry.forEach { $0(succeeded, 0.0) }
                    strongSelf.requestsToRetry.removeAll()
                }
            }
        } else {
            completion(false, 0.0)
        }
    }
    
    // MARK: - Private - Refresh Token
    
    /// Refreshes access token with provided credentials
    ///
    /// - Parameter completion: RefreshCompletion handler block
    private func refreshToken(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }
        isRefreshing = true

        sessionManager.request(RequestRouter.oauthToken(
            // TODO: Need to encrypt hardcoded values
            parameters: [Constants.API.PARAMETER.grantType: "password",
                         Constants.API.PARAMETER.username: "carlos@nimbl3.com",
                         Constants.API.PARAMETER.password: "antikera"]))
            .responseData { [weak self] response in
                guard let strongSelf = self else { return }
                
                if let data = response.data {
                    let token = try! JSONDecoder().decode(Token.self, from: data)
                    debugPrint("New Access Token: \(token.accessToken)")
                    completion(true, token.accessToken)
                } else {
                    completion(false, nil)
                }
                
                strongSelf.isRefreshing = false
        }
    }
    
}
