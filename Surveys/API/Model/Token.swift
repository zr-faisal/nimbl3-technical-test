//
//  Token.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 28/11/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

struct Token: Codable {
    
    var accessToken: String
    var tokenType: String
    var expiresIn: Int
    var createdAt: Int
    
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case tokenType = "token_type"
        case expiresIn = "expires_in"
        case createdAt = "created_at"
    }
    
}
