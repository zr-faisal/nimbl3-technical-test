//
//  Survey.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 7/12/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

struct Survey: Codable {
    
    var coverImage: String
    var name: String
    var description: String
    
    enum CodingKeys: String, CodingKey {
        case coverImage = "cover_image_url"
        case name = "title"
        case description = "description"
    }
    
}
