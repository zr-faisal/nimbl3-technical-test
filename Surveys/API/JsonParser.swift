//
//  JsonParser.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 2/12/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation

class JsonParser {
    
    /// Parse JSON response and returns a list of Survey
    ///
    /// - Parameter jsonResponse: jsonResponse from server
    /// - Returns: List of Survey
    class func parseSurveys(jsonResponse: Data) -> [Survey]? {
        do {
            let surveys =  try JSONDecoder().decode([Survey].self, from: jsonResponse)
            return surveys
        } catch let jsonError {
            debugPrint("Error - parseSurveys: ", jsonError)
            return nil
        }
    }
    
}
