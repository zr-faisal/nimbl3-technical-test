//
//  Router.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 29/11/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import Foundation
import Alamofire

enum RequestRouter: URLRequestConvertible {
    
    case allSurveys()
    case surveys(page: Int, perPage: Int)
    case oauthToken(parameters: Parameters)
    
    static let baseURLString = Constants.API.baseUrl
    
    /// Determines request type for each API service
    var method: HTTPMethod {
        switch self {
        case .allSurveys:
            return .get
        case .surveys:
            return .get
        case .oauthToken:
            return .post
        }
    }
    
    /// Generates path compenents with parameters
    var pathComponent: (path: String, parameters: Parameters?) {
        switch self {
        case .allSurveys():
            return (Constants.API.PATH.surveys, nil)
        case .surveys(let page, let perPage):
            return (Constants.API.PATH.surveys, ["page": page, "per_page": perPage])
        case .oauthToken(let parameters):
            return (Constants.API.PATH.oauth, parameters)
        }
    }
    
    // MARK: URLRequestConvertible
    
    /// Returns final request with path components and parameters
    ///
    /// - Returns: urlRequest
    /// - Throws: 
    func asURLRequest() throws -> URLRequest {
        let url = try RequestRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(pathComponent.path))
        urlRequest = try URLEncoding.default.encode(urlRequest, with: pathComponent.parameters)
        
        return urlRequest
    }
    
}
