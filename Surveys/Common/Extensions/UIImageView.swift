//
//  UIImageView.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 8/12/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit
import Nuke

extension UIImageView {
    
    /// Loads high-resulation image and sets to invoking UIImageView
    ///
    /// - Parameter imageUrl: Url string for server image
    func loadHighResultImage(imageUrl: String) {
        if imageUrl != "No Image" {
            let highResImage = "\(imageUrl)l"
            Manager.shared.loadImage(with: URL.init(string: highResImage)!, into: self)
        }
    }
    
}
