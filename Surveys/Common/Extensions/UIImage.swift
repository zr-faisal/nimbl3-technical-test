//
//  UIImage.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 8/12/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

extension UIImage {
    
    /// Creates a solid-circle image
    ///
    /// - Parameters:
    ///   - diameter: diameter for circle
    ///   - color: color for circle
    /// - Returns: UIImage of solid-circle
    class func solidCircle(diameter: CGFloat, color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: diameter, height: diameter), false, 0)
        let context = UIGraphicsGetCurrentContext()!
        context.saveGState()
        
        let rect = CGRect(x: 0, y: 0, width: diameter, height: diameter)
        context.setFillColor(color.cgColor)
        context.fillEllipse(in: rect)
        
        context.restoreGState()
        let circleImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return circleImage
    }
    
    /// Creates a hollow-circle image
    ///
    /// - Parameters:
    ///   - diameter: diameter for circle
    ///   - color: color for circle
    /// - Returns: UIImage of hollow-circle
    class func hollowCircle(diameter: CGFloat, color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: diameter, height: diameter), false, 0)
        let context = UIGraphicsGetCurrentContext()!
        context.saveGState()
        
        let lineWidth: CGFloat = 1
        let rect = CGRect(x: lineWidth, y: lineWidth, width: diameter - 2*lineWidth, height: diameter - 2*lineWidth)
        context.setFillColor(UIColor.clear.cgColor)
        context.setStrokeColor(color.cgColor)
        context.setLineWidth(lineWidth)
        context.strokeEllipse(in: rect)
        context.drawPath(using: .fillStroke)
        
        context.restoreGState()
        let circleImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return circleImage
    }
    
}
