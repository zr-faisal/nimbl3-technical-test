//
//  KeychainHelper.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 3/12/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import KeychainAccess

class KeychainHelper {
    
    /// Stores value with a specific key in Keychain
    ///
    /// - Parameters:
    ///   - key: key for object
    ///   - value: value for object
    class func set(key: String, value: String) {
        let keychain = Keychain(service: Constants.KeyChain.bundleIdentifier)
        keychain[key] = value
    }
    
    /// Retrives value for a specific key
    ///
    /// - Parameter key: key for object
    /// - Returns: String representation of value
    class func get(key: String) -> String {
        let keychain = Keychain(service: Constants.KeyChain.bundleIdentifier)
        return keychain[string: key] ?? Constants.KeyChain.accessTokenDefaultValue
    }
    
}
