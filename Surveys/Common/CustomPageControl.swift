//
//  CustomPageControl.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 3/12/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class CustomPageControl: UIPageControl {
    
    let activeImage:UIImage = UIImage.solidCircle(diameter: 10, color: UIColor.white)
    let inactiveImage:UIImage = UIImage.hollowCircle(diameter: 10, color: UIColor.white)
    
    /// Initial configuration
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.pageIndicatorTintColor = UIColor.clear
        self.currentPageIndicatorTintColor = UIColor.clear
        self.clipsToBounds = false
    }
    
    /// Updates page indicators base on currentPage
    func updateDots() {
        var i = 0
        for view in self.subviews {
            if let imageView = self.imageForSubview(view) {
                if i == self.currentPage {
                    imageView.image = self.activeImage
                } else {
                    imageView.image = self.inactiveImage
                }
                i = i + 1
            } else {
                var dotImage = self.inactiveImage
                if i == self.currentPage {
                    dotImage = self.activeImage
                }
                view.clipsToBounds = false
                view.addSubview(UIImageView(image:dotImage))
                i = i + 1
            }
        }
    }
    
    
    /// Returns image for each page indicator
    ///
    /// - Parameter view: view of page indicator
    /// - Returns: UIImageView
    fileprivate func imageForSubview(_ view:UIView) -> UIImageView? {
        var dot:UIImageView?
        
        if let dotImageView = view as? UIImageView {
            dot = dotImageView
        } else {
            for foundView in view.subviews {
                if let imageView = foundView as? UIImageView {
                    dot = imageView
                    break
                }
            }
        }
        
        return dot
    }
}
