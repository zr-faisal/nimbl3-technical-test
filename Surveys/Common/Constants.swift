//
//  Constants.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 28/11/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

struct Constants {
    
    struct API {
        static let baseUrl = "https://nimbl3-survey-api.herokuapp.com"
        
        struct PATH {
            static let surveys = "/surveys.json"
            static let oauth = "/oauth/token"
        }
        
        struct PARAMETER {
            static let grantType = "grant_type"
            static let username = "username"
            static let password = "password"
        }
        
        struct HEADER {
            struct AUTHORIZATION {
                static let prefix = "Bearer "
                static let name = "Authorization"
            }
        }
    }
    
    struct JsonKey {
        static let image_url = "image_url"
        static let short_text = "short_text"
        static let text = "text"
    }
    
    struct Notification {
        static let reload = "reload"
        static let indicators = "indicators"
        static let currentPage = "currentPage"
    }
    
    struct KeyChain {
        static let bundleIdentifier = "com.nimbl3.Surveys"
        static let accessTokenKey = "accessTokenKey"
        static let accessTokenDefaultValue = "d9584af77d8c0d6622e2b3c554ed520b2ae64ba0721e52daa12d6eaa5e5cdd93"
    }
    
}
