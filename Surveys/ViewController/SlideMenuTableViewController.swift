//
//  SlideMenuTableViewController.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 27/11/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class SlideMenuTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("SlideMenuTableVC")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Instiansiation
    
    /// Instantiates a new SlideMenuTableViewController from Storyboard
    ///
    /// - Returns: SlideMenuTableViewController
    static func instantiate() -> SlideMenuTableViewController {
        return UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "SlideMenuTableViewController") as! SlideMenuTableViewController
    }
    
}
