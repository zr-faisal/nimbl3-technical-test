//
//  QuestionViewController.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 3/12/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class SurveyViewController: UIViewController {
    
    @IBOutlet weak var imageViewCoverImage: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    var survey: Survey?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initiate UI
        imageViewCoverImage.loadHighResultImage(imageUrl: (survey?.coverImage)!)
        labelName.text = survey?.name
        labelDescription.text = survey?.description
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Instiansiation
    
    /// Instantiates a new QuestionViewController from Storyboard
    ///
    /// - Returns: QuestionViewController
    static func instantiate() -> SurveyViewController {
        return UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "QuestionViewController") as! SurveyViewController
    }
    
}
