//
//  TakeSurveyViewController.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 3/12/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class TakeSurveyViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Instiansiation
    
    /// Instantiates a new TakeSurveyViewController from Storyboard
    ///
    /// - Returns: TakeSurveyViewController
    static func instantiate() -> TakeSurveyViewController {
        return UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "TakeSurveyViewController") as! TakeSurveyViewController
    }
    
    // MARK: IBAction
    
    /// Returns to MainViewController on Back Button click
    ///
    /// - Parameter sender: UIBarButton
    @IBAction func onClickBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
