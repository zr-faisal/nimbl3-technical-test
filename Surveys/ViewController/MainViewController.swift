//
//  MainViewController.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 27/11/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var pageControl: CustomPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup vertical PageControl
        pageControl.numberOfPages = 0
        pageControl.transform = CGAffineTransform(rotationAngle: (CGFloat(Double.pi / 2)))
        pageControl.updateDots()
        
        subscribeEvents(subscribe: true)
    }
    
    /// Subscribes notification events on viewDidAppear
    ///
    /// - Parameter animated: animation flag
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        subscribeEvents(subscribe: true)
    }
    
    /// Unubscribes notification events on viewDidDisappear
    ///
    /// - Parameter animated: animation flag
    override func viewDidDisappear(_ animated: Bool) {
        subscribeEvents(subscribe: false)
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Instiansiation
    
    /// Instantiates a new MainViewController from Storyboard
    ///
    /// - Returns: MainViewController
    static func instantiate() -> MainViewController {
        return UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
    }
    
    // MARK: IBAction
    
    /// Sends notification for force-reload to QuestionsPageViewController
    ///
    /// - Parameter sender: UIBarButton
    @IBAction func onClickReloadButton(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notification.reload), object: nil)
    }
    
    // MARK: Private Functions
    
    /// Subscribes and unsubscribes notifications based on flag
    ///
    /// - Parameter subscribe: subscription flag
    func subscribeEvents(subscribe: Bool) {
        if subscribe {
            // Subscribe to indicators notification
            NotificationCenter.default.addObserver(self, selector: #selector(self.setIndicators), name: NSNotification.Name(rawValue: Constants.Notification.indicators), object: nil)
            // Subscribe to currentPage notification
            NotificationCenter.default.addObserver(self, selector: #selector(self.setCurrentPage), name: NSNotification.Name(rawValue: Constants.Notification.currentPage), object: nil)
        } else {
            // Unubscribe to reload notification
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    // MARK: Notification
    
    /// Sets number of indicators from notification data
    ///
    /// - Parameter notification: incoming notification with data
    @objc func setIndicators(notification: Notification) {
        if let numberOfPages = notification.object as? Int {
            self.pageControl.numberOfPages = numberOfPages
            self.pageControl.currentPage = 0
            self.pageControl.updateDots()
        }
    }
    
    /// Sets current page indicator from notification data
    ///
    /// - Parameter notification: incoming notification with data
    @objc func setCurrentPage(notification: Notification) {
        if let currentPage = notification.object as? Int {
            self.pageControl.currentPage = currentPage
            self.pageControl.updateDots()
        }
    }
    
}

