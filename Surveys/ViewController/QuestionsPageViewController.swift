//
//  QuestionsPageViewController.swift
//  Surveys
//
//  Created by Zahidur Rahman Faisal on 3/12/17.
//  Copyright © 2017 Nimbl3. All rights reserved.
//

import UIKit

class QuestionsPageViewController: UIPageViewController {
    
    var pages = [UIViewController]()
    
    // Tracks current index
    private var currentIndex: Int?
    private var pendingIndex: Int?
    
    // Tracks loading state
    private var isLoading = false
    private var activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadSurveys()
    }
    
    /// Subscribes notification events and user action on viewDidAppear if not loading data
    ///
    /// - Parameter animated: animation flag
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isLoading {
            subscribeEvents(subscribe: true)
        }
    }
    
    /// Unubscribes notification events and user action on viewDidDisappear
    ///
    /// - Parameter animated: animation flag
    override func viewDidDisappear(_ animated: Bool) {
        subscribeEvents(subscribe: false)
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Instiansiation
    
    /// Instantiates a new QuestionsPageViewController from Storyboard
    ///
    /// - Returns: QuestionsPageViewController
    static func instantiate() -> QuestionsPageViewController {
        return UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "QuestionsPageViewController") as! QuestionsPageViewController
    }
    
    // MARK: Private Functions
    
    /// Loads servey data and populates UI
    @objc private func loadSurveys() {
        isLoading = true
        showLoader()
        subscribeEvents(subscribe: false)
        
        ApiRequestHandler().sessionManager
            .request(RequestRouter.allSurveys())
            .validate()
            .responseData{ (response) in
                switch response.result {
                case .success(let data):
                    if let surveys = JsonParser.parseSurveys(jsonResponse: data), surveys.count > 0 {
                        var tag = 0
                        for survey in surveys {
                            let surveyViewController = SurveyViewController.instantiate()
                            surveyViewController.survey = survey
                            surveyViewController.view.tag = tag
                            self.pages.append(surveyViewController)
                            tag += 1
                        }
                        self.setViewControllers([self.pages.first!], direction: .forward, animated: false, completion: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notification.indicators), object: surveys.count)
                    } else {
                        debugPrint("Request has no data or parsing failed!")
                        // TODO: Need to show Alert for error
                    }
                case .failure(let error):
                    debugPrint("Request failed with error: \(error)")
                    // TODO: Need to show Alert for error
                }
                
                self.isLoading = false
                self.hideLoader()
                self.subscribeEvents(subscribe: true)
            }
    }
    
    /// Shows loader when loading data from server
    func showLoader() {
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    /// Hides loader when loading data from server
    func hideLoader() {
        if activityIndicator.isAnimating {
            activityIndicator.stopAnimating()
        }
    }
    
    /// Subscribes and unsubscribes notifications and user actions based on flag
    ///
    /// - Parameter subscribe: subscription flag
    func subscribeEvents(subscribe: Bool) {
        if subscribe {
            // Enable browsing surveys
            self.dataSource = self
            self.delegate = self
            // Subscribe to reload notification
            NotificationCenter.default.addObserver(self, selector: #selector(self.loadSurveys), name: NSNotification.Name(rawValue: Constants.Notification.reload), object: nil)
        } else {
            // Disable browsing surveys
            self.dataSource = nil
            self.delegate = nil
            // Unubscribe to reload notification
            NotificationCenter.default.removeObserver(self)
        }
    }
    
}

// MARK: UIPageViewControllerDataSource

extension QuestionsPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        if currentIndex == 0 {
            return nil
        }
        let previousIndex = abs((currentIndex - 1) % pages.count)
        
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.index(of: viewController)!
        if currentIndex == pages.count - 1 {
            return nil
        }
        let nextIndex = abs((currentIndex + 1) % pages.count)
        
        return pages[nextIndex]
    }
    
}

// MARK: UIPageViewControllerDelegate

extension QuestionsPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed, let currentPage = pageViewController.viewControllers?.first?.view.tag {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notification.currentPage), object: currentPage)
        }
    }
    
}
